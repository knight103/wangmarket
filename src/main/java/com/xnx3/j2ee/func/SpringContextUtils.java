package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.SpringUtil;

/**
 * SpringContext相关，如获取spring中的bean
 * Author：Mr.X
 * @deprecated 已废弃，请使用 {@link SpringUtil}
 */
public class SpringContextUtils extends SpringUtil {

}